# Pull alpine open JDK image
FROM openjdk:18-alpine3.13
# Make sure user is root, this will allow changing permissins.
USER root
# Create app dir and change to it
WORKDIR /usr/maven_project
# Add argument during build.
ARG TAG
# To use tag in the CMD 
ENV ARTIFACT=${TAG}
# Prevent all jar files to be moved into the image, 
COPY ./target/${ARTIFACT}.jar .
# Add all permesission so any user can run jar file.
RUN chmod 777 -R /usr/maven_project
# Allow adding env vars during running the contianer.
# h2 database is the default, but can be changed using docker-compose or using docker contianer run.
# default springboot port is 8090, can be changed using docker-compose or using docker contianer run.
ENV PROFILE=h2
ENV PORT=8090
# Using a Non-Root user inside each Container. 
RUN adduser -D -g '' u997
USER u997
# Command to be excuted when running the container.
CMD java -jar -Dserver.port=${PORT} -Dspring.profiles.active=${PROFILE} ${ARTIFACT}.jar