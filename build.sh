#!/bin/bash

GIT_DATE=$(git show -s --date=format:'%y%m%d' --format=%cd)
GIT_COMMIT=$(git rev-parse --short=8 HEAD)
TAG="$GIT_DATE-$GIT_COMMIT"


docker build -t u997/java_app:$TAG --build-arg TAG=assignment-$TAG .


